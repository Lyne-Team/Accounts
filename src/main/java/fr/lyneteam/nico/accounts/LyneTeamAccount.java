package fr.lyneteam.nico.accounts;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import fr.lyneteam.nico.accounts.LyneTeamAccountDataError.Responce;
import fr.lyneteam.nico.oson.JsonObject;
import fr.lyneteam.nico.oson.JsonParser;

public class LyneTeamAccount {
	private final LyneTeamAccounts accounts;
	private final String id;
	private LyneTeamAccountInfo info;
	
	public LyneTeamAccount(LyneTeamAccounts accounts, String id) {
		this.accounts = accounts;
		this.id = id;
		try {
			this.reloadAccountsData();
		} catch (LyneTeamAccountDataError e) {
			// Ignore
		}
	}
	
	public final LyneTeamAccounts getAuthClass() {
		return this.accounts;
	}
	
	public final String getID() {
		return this.id;
	}
	
	public final LyneTeamAccountInfo getInfo() {
		return this.info;
	}

	public final LyneTeamAccountInfo reloadAccountsData() throws LyneTeamAccountDataError {
		try {
			URL url = new URL(this.accounts.getURL() + "get=" + this.id + "&ip=" + this.accounts.getIP());
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8")));
			String responce = reader.readLine();
			try {
				reader.close();
			} catch (Exception exception) {
				// Ignore
			}
			try {
				int id = Integer.parseInt(responce);
				throw new LyneTeamAccountDataError(Responce.getResponceByID(id));
			} catch (LyneTeamAccountDataError exception) {
				throw exception;
			} catch (Exception exception) {
				JsonObject object = (JsonObject) new JsonParser().parse(responce);
				this.info = new LyneTeamAccountInfo(object);
				return this.info;
			}
		} catch (LyneTeamAccountDataError exception) {
			throw exception;
		} catch (Exception exception) {
			throw new LyneTeamAccountDataError(exception);
		}
	}
	
	public final JsonObject getData(String key) throws LyneTeamAccountDataError {
		try {
			URL url = new URL(this.accounts.getURL() + "data=" + this.id + "&ip=" + this.accounts.getIP() + "&key=" + key);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8")));
			String responce = reader.readLine();
			try {
				reader.close();
			} catch (Exception exception) {
				// Ignore
			}
			try {
				int id = Integer.parseInt(responce);
				throw new LyneTeamAccountDataError(Responce.getResponceByID(id));
			} catch (LyneTeamAccountDataError exception) {
				throw exception;
			} catch (Exception exception) {
				return (JsonObject) new JsonParser().parse(responce);
			}
		} catch (LyneTeamAccountDataError exception) {
			throw exception;
		} catch (Exception exception) {
			throw new LyneTeamAccountDataError(exception);
		}
	}
}