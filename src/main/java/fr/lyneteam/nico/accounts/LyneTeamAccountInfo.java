package fr.lyneteam.nico.accounts;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.lyneteam.nico.oson.JsonObject;

public class LyneTeamAccountInfo {
	private final String username, firstName, lastName, mail, country, city, address;
	private final Date date;
	private final long phone, phone1;
	private final int postal;
	private final LyneTeamAccountStatus status;
	
	public LyneTeamAccountInfo(JsonObject object) {
		this.username = object.get("username").getAsString();
		this.firstName = object.get("first_name").getAsString();
		this.lastName = object.get("last_name").getAsString();
		this.mail = object.get("mail").getAsString();
		this.country = object.get("country").getAsString();
		this.city = object.get("city").getAsString();
		this.address = object.get("address").getAsString();
		Date date = new Date((long) 0);
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(object.get("date").getAsString());
		} catch (Exception exception) {
			// Ignore
		}
		this.date = date;
		this.phone = object.get("phone").getAsLong();
		this.phone1 = object.get("phone1").getAsLong();
		this.postal = object.get("postal").getAsInt();
		this.status = LyneTeamAccountStatus.getByDatabaseName(object.get("status").getAsString());
	}

	public final String getUsername() {
		return this.username;
	}

	public final String getFirstName() {
		return this.firstName;
	}

	public final String getLastName() {
		return this.lastName;
	}

	public final String getMail() {
		return this.mail;
	}

	public final String getCountry() {
		return this.country;
	}

	public final String getCity() {
		return this.city;
	}

	public final String getAddress() {
		return this.address;
	}

	public final Date getDate() {
		return this.date;
	}

	public final long getPhone() {
		return this.phone;
	}

	public final long getPhone1() {
		return this.phone1;
	}

	public final int getPostal() {
		return this.postal;
	}

	public final LyneTeamAccountStatus getStatus() {
		return this.status;
	}
}