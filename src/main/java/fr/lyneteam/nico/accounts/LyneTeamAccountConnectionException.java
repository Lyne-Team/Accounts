package fr.lyneteam.nico.accounts;

public class LyneTeamAccountConnectionException extends Exception {
	private static final long serialVersionUID = 287804450359775867L;
	private final Responce responce;
	private final Exception exception;
	
	public LyneTeamAccountConnectionException(Responce responce, Exception exception) {
		super(responce.toString());
		this.responce = responce;
		this.exception = exception;
	}
	
	public LyneTeamAccountConnectionException(Responce responce) {
		this(responce, null);
	}
	
	public LyneTeamAccountConnectionException(Exception exception) {
		this(Responce.INTERNET_ERROR, exception);
	}

	public final Responce getResponce() {
		return this.responce;
	}
	
	public final Exception getException() {
		if (this.exception == null) return this; else return this.exception;
	}
	
	public enum Responce {
		INTERNET_ERROR(-99, "Could not connect to the Lyne-Team authentication server."),
		SERVER_ERROR(-1, "Internal server error. Try again later."),
		DATA_USERNAME_ERROR(0, "No username sended."),
		DATA_PASSWORD_ERROR(1, "No password sended."),
		ACCOUNT_NOT_FOUND(2, "Account not found."),
		INCORRECT_PASSWORD(3, "Incorrect password.");
		
		public static final Responce getResponceByID(int id) {
			for (Responce responce : values()) if (responce.id == id) return responce;
			return null;
		}
		
		private final int id;
		private final String message;
		
		private Responce(int id, String message) {
			this.id = id;
			this.message = message;
		}
		
		@Override
		public String toString() {
			return this.message;
		}
	}
}