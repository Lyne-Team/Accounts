package fr.lyneteam.nico.accounts;

public class LyneTeamAccountDataError extends Exception {
	private static final long serialVersionUID = 287804450359775868L;
	private final Responce responce;
	private final Exception exception;
	
	public LyneTeamAccountDataError(Responce responce, Exception exception) {
		super(responce.toString());
		this.responce = responce;
		this.exception = exception;
	}
	
	public LyneTeamAccountDataError(Responce responce) {
		this(responce, null);
	}
	
	public LyneTeamAccountDataError(Exception exception) {
		this(Responce.INTERNET_ERROR, exception);
	}

	public final Responce getResponce() {
		return this.responce;
	}
	
	public final Exception getException() {
		if (this.exception == null) return this; else return this.exception;
	}
	
	public enum Responce {
		INTERNET_ERROR(-99, "Could not connect to the Lyne-Team authentication server."),
		DATA_PARSE_ERROR(-2, "Server data parsing error."),
		ACCOUNT_ERROR(-1, "This account doesn't work. Please reconnect.");
		
		public static final Responce getResponceByID(int id) {
			for (Responce responce : values()) if (responce.id == id) return responce;
			return null;
		}
		
		private final int id;
		private final String message;
		
		private Responce(int id, String message) {
			this.id = id;
			this.message = message;
		}
		
		@Override
		public String toString() {
			return this.message;
		}
	}
}