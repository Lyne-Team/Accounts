package fr.lyneteam.nico.accounts;

public enum LyneTeamAccountStatus {
	INDIVIDUAL("particulier"),
	ASSOCIATION("association"),
	BUSINESS("entreprise");
	
	public static final LyneTeamAccountStatus getByDatabaseName(String name) {
		for (LyneTeamAccountStatus status : values()) if (status.name.equals(name)) return status;
		return null;
	}
	
	private final String name;
	
	private LyneTeamAccountStatus(String name) {
		this.name = name;
	}
}