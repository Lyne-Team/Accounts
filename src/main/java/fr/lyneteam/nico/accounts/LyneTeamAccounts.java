package fr.lyneteam.nico.accounts;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import fr.lyneteam.nico.accounts.LyneTeamAccountConnectionException.Responce;

public class LyneTeamAccounts {
	public static void main(String[] args) {
		System.err.println("This is an api.");
	}
	
	private final String username, password, ip;
	
	public LyneTeamAccounts(String username, String password) {
		this.username = username;
		this.password = password;
		String ip = "127.0.0.1";
		try {
			URL url = new URL("https://lyneteam.eu/ip.php");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8")));
			ip = reader.readLine();
			reader.close();
		} catch (Exception exception) {
			// Ignore
		}
		this.ip = ip;
	}
	
	public final LyneTeamAccount getAccount(String username, String password) throws LyneTeamAccountConnectionException {
		try {
			URL url = new URL(this.getURL() + "connect=true&username=" + URLEncoder.encode(username, "UTF-8") + "&password=" + URLEncoder.encode(password, "UTF-8"));
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8")));
			String responce = reader.readLine();
			try {
				reader.close();
			} catch (Exception exception) {
				// Ignore
			}
			try {
				int id = Integer.parseInt(responce);
				throw new LyneTeamAccountConnectionException(Responce.getResponceByID(id));
			} catch (LyneTeamAccountConnectionException exception) {
				throw exception;
			} catch (Exception exception) {
				String[] data = responce.split("_");
				if (data.length == 2) return new LyneTeamAccount(this, URLEncoder.encode(data[0], "UTF-8")); else throw new LyneTeamAccountConnectionException(Responce.SERVER_ERROR);
			}
		} catch (LyneTeamAccountConnectionException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new LyneTeamAccountConnectionException(exception);
		}
	}
	
	protected final String getURL() throws UnsupportedEncodingException {
		return "https://lyneteam.eu/account.php?api=" + URLEncoder.encode(this.username, "UTF-8") + "&apipass=" + URLEncoder.encode(this.password, "UTF-8") + "&";
	}

	public final String getIP() {
		return this.ip;
	}
}